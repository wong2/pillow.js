var fs = require('fs');
var path = require('path');
var wd = require('wd');
var gm = require('gm');

var browser = wd.promiseChainRemote('http://localhost:9515/');

module.exports.capture = function(htmlPath, outPath, size, callback) {
  var url = 'file://' + path.resolve(htmlPath);
  browser
    .init({browserName:'chrome'})
    .get(url)
    .maximize()
    .takeScreenshot()
    .then(function(base64Data) {
      fs.writeFileSync(outPath, base64Data, 'base64');
      gm(outPath).crop(size.width, size.height, 0, 0).write(outPath, callback);
    })
    .fail(callback)
    .fin(function() {
      return browser.quit();
    })
    .done();
};
