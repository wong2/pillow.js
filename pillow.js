var fs = require('fs');
var path = require('path');
var tmp = require('tmp');
var jade = require('jade');
var sizeOf = require('image-size');
var chrome2png = require('./chrome2png');

module.exports.render = function(dest, options, callback) {
  var bgSize = sizeOf(options.background);
  var template;
  if (options.template) {
    template = options.template;
  } else {
    template = path.join(__filename, '../example/tmpl.jade');
  }
  var html = jade.renderFile(template, {
    mainText: options.mainText,
    startButtonText: options.startButtonText,
    background: path.resolve(options.background),
    bgSize: bgSize,
    customCSS: options.customCSS || ''
  });
  tmp.file({postfix: '.html'}, function(err, tmp_path) {
    console.log('tmp path: ', tmp_path);
    fs.writeFileSync(tmp_path, html);
    chrome2png.capture(tmp_path, dest, bgSize, callback);
  });
};
