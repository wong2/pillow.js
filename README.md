1. 通过 `git clone https://wong2@bitbucket.org/wong2/pillow.js.git` 得到这份代码
2. 进入 `pillow.js` 目录，执行 `npm install` 安装依赖
3. 执行 `brew install graphicsmagick` 安装gm
4. 执行 `brew install chromedriver` 安装chromedriver

需要提供一个目标目录，参考 `example`，下面有 `config.json` 这个配置文件，和 一个 jade 模板，还有 背景图片。

在终端里运行 `chromedriver`，
然后执行 `node pillow.js 目标目录`，如 `node pillow.js example`，就可以了，生成的图片在目标目录中。
