var fs = require('fs');
var path = require('path');
var each = require('each-series');
var logSymbols = require('log-symbols');
var pillow = require('./pillow');

if (process.argv.length < 3) {
  console.log('Usage: node pillow.js dir [language]');
  process.exit();
}

var dir = process.argv[2]
  , lang = process.argv[3];

process.chdir(dir);

var config = JSON.parse(fs.readFileSync('config.json'));

if (lang) {
  var langs = [lang];
} else {
  var langs = Object.keys(config.text);
}

each(langs, function(lang, index, done) {
  var texts = config.text[lang];
  var imageName = lang + '.png';
  pillow.render(imageName, {
    template: config.template,
    mainText: texts[0],
    startButtonText: texts[1],
    background: config.background,
    customCSS: config.css ? config.css[lang] : ''
  }, function(err) {
    if (!err) {
      console.log(logSymbols.success, 'Screenshot generated: ' + imageName);
    }
    done(err);
  })
}, function(err) {
  if (err) {
    console.log(logSymbols.error, err);
  }
});
